<?php


class Db {
    
    private $pdo; 

    public function __construct() {
        
        $paramsPath = ROOT . 'config/db_params.php';
        $params = include($paramsPath);

        $this->pdo = new \PDO(
            'mysql:host=' . $params['host'] . ';dbname=' . $params['dbname'],
            $params['user'],
            $params['password']
        );
        $this->pdo->exec('SET NAMES UTF8');
    }
    
    public function query(string $sql, $params = []): ?array
    {
        
        foreach ($params as $key => $value) {
            $params[$key] = strip_tags($value);
        }
        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute($params);
        if (false === $result) {
            return null;
        }

        return $stmt->fetchAll();
    }
}

