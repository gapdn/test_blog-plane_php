<?php

class View {
  
    private $viewPath;

    public function __construct(string $viewPath)
    {
        $this->viewPath = $viewPath;
    }

    public function renderHtml(string $viewName, array $vars = [])
    {
        extract($vars);

        include $this->viewPath . '/' . $viewName;
    }
    
}
