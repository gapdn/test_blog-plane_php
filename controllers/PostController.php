<?php


include_once (ROOT . 'models/Post.php');
include_once (ROOT . 'controllers/AppController.php');
include_once (ROOT . 'models/Author.php');
include_once (ROOT . 'models/Comment.php');

class PostController extends AppController {
     
    public function __construct() {
        parent::__construct();
    }
    
    public function actionIndex() {
        
        $post = new Post;
        $result = $post->getPosts();
        
        if (empty($result)) {
            $message = 'Nobody posted yet';
            $this->view->renderHtml('post/index.php', ['message' => $message]);
            return;
        }
        
        $posts = [];
        foreach ($result as $p) {
            $posts[] = [
                'id' => $p['id'],
                'content' => $post->getShortText($p['content']), 
                'name' => $post->getAuthorName($p['author_id']),
                'date' => $p['created_at'],
                'comments' => $post->getCountComments($p['id']),
            ];
        }
        
        $fresult = $post->getFavoritePosts();
        $fposts =[];
        foreach ($fresult as $row) {
            $fposts[] = $post->getShortText($row['content']);
        }
        
        
        $this->view->renderHtml('post/index.php', ['posts' => $posts, 'fposts' => $fposts]);
        
        return true;
    }
    
    public function actionView($id) {
        
        if (empty($id)) {
        
            //TODO: set message post with $id not found
            header('Location: /');
            return;
        }
        $post = new Post();
        $rpost = $post->getPostById($id);
        $post_content = $rpost['content'];
        $date_create = $rpost['created_at'];
        $author_id = $rpost['author_id'];

        $author = new Author();
        $author_name = $author->getName($author_id);
        $comment = new Comment();
        $comments = $comment->getComments($id);
        $comments_content = [];

        if (!empty($comments)) {
            foreach ($comments as $c) {
                $comments_content[] = [
                    'content' => $c['content'],
                    'author' => $comment->getAuthor($c['id']) ?? '',
                    'date' => $c['created_at'],
                ];

            }

        }
        $this->view->renderHtml('post/view.php', [
            'post_id' => $id ?? '',
            'content' => $post_content ?? '',
            'author_name' => $author_name ?? '',
            'date_create' => $date_create ?? '',
            'comments' => $comments_content ?? [],
        ]);
            
        return true;
    }
    
    public function actionCreate() {
        
        if (empty($_POST)) {
            $this->view->renderHtml('post/create.php');
            
            return true;
        }
        
        $name = $_POST['name'];
        $content = $_POST['content'];

        $post = new Post();
        $author = new Author();
        $author_id = $author->getId($name);

        if (empty($author_id)) {
            $author->create($name);
            $author_id = $author->getId($name);
        }

        $post->create($author_id, $content);
        
        header('Location: /');

        return true;
        
    }
    
    public function actionAbout() {
        
        $this->view->renderHtml('post/about.php');
        
        return true;
    }
    
}
