<?php


include_once (ROOT . 'models/Post.php');
include_once (ROOT . 'controllers/AppController.php');
include_once (ROOT . 'models/Author.php');
include_once (ROOT . 'models/Comment.php');

class CommentController extends AppController {
    
    

    public function __construct() {
        parent::__construct();
    }
    
    
    public function actionCreate() {
        
        if (empty($_POST)) {
            header('Location: /');
            
            return;
        }
        $content = $_POST['content'] ?? '';
        $post_id = $_POST['post_id'] ?? '';
        $author_name = $_POST['name'] ?? '';
        $author = new Author();
        $author_id = $author->getId($author_name);

        if (empty($author_id)) {
            $author->create($author_name);
            $author_id = $author->getId($author_name) ?? '';
        }

        $comment = new Comment();
        $comment->create($post_id, $content, $author_id);
        
        header('Location: /post/' . $post_id);
        return true;
    }

}