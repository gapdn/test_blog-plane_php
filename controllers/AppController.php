<?php

require_once (ROOT . 'components/Db.php');
require_once (ROOT . 'components/View.php');

class AppController {
    
    protected $db;
    
    protected $view;


    public function __construct() {
        
        $this->view = new View(VIEW);
        $this->db = new Db();
    }
    
    
}
