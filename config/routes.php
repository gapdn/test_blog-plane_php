<?php

return [
    'comment/create' => 'comment/create',
    'post/([0-9]+)' => 'post/view/$1',
    'post' => 'post/index',
    'about' => 'post/about',
    'create' => 'post/create',
    '' => 'post/index',
    
];
