Техническое задание
-------------------

Необходимо, на чистом PHP, реализовать публичный мини-блог,  в котором любой гость может создавать записи, 
а другие гости имеют право комментировать записи. Авторизация не нужна.

В функционале обязаны присутствовать две страницы: список последних записей и одна запись.

На странице списка последних записей выводится слайдер “популярное” из 5 самых коментируемых записей. 
Ниже - записи в порядке давности публикации, 
которые должны содержать имя автора, 
короткий текст публикации(обрезка 100 символов), 
дату публикации, 
количество комментариев и ссылку для перехода на полную запись. 
Так же на этой странице должна находиться форма для отправки публикации, в которой указываются имя пользователя и текст публикации.

На странице полной публикации выводится всё то же, что и в короткой публикации, 
только текст публикации должен быть полным, 
а так же комментарии к этой публикации и форма добавления нового комментария в котором указывается имя автора и текст публикации.

Выбор способа хранения информации и визуальной составляющей на усмотрение кандидата.

Предоставить задание в открытом репозитории, с доступной документацией по установке и настройке. 
------------------------------------------------------------------------------------------------


Установка
---------
1. Клонирование репозитория
git clone https://gapdn@bitbucket.org/gapdn/v_jet.git

2. Настройка соединения с БД
добавить в проект файл db_params.php (каталог /config)

содержимое файла:

<?php

return [
    'user' => 'user_name',
    'password' => 'user_password',
    'host' => 'localhost',
    'dbname' => 'your_db_name',
];

3. Создание БД:

CREATE DATABASE `your_db_name` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE your_db_name;
CREATE USER 'user_name'@'localhost' IDENTIFIED BY 'user_password';
GRANT ALL PRIVILEGES ON your_db_name.* TO 'user_name'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE `author` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `post` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`author_id` int(11) NOT NULL,
	`content` text NOT NULL,
	`created_at` timestamp DEFAULT current_timestamp,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `comment` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`author_id` int(11) NOT NULL,
	`post_id` int(11) NOT NULL,
	`content` varchar(255) NOT NULL,	
	`created_at` timestamp DEFAULT current_timestamp,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

----------------

Проект развернут на сервере с:
------------------------------
- Ubuntu 18.04
- PHP 7.2.7
- nginx/1.14.0
- MariaDB 6