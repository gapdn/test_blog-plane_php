<?php

require_once (ROOT . 'models/AppModel.php');

class Comment extends AppModel{
    
    public function __construct() {
        parent::__construct();
    }
    
    
    public function getComments($post_id) {
        
        $sql = 'SELECT * from comment WHERE post_id = :id';
        $comments = $this->db->query($sql, [':id' => $post_id]);
        
        if (!empty($comments)) {
            return $comments;
        }
        
        return null;
    }

    public function getContent($id) {
        
        $sql = 'SELECT * from comment WHERE id = :id';
        $comment = $this->db->query($sql, [':id' => $id]);
        if (!empty($comment)) {
            $content = $comment[0]['content'];
            return $content;
        }
        
        return null;
    }
    
     public function getAuthor($id) {
        
        $sql = 'SELECT * from comment WHERE id = :id';
        $comment = $this->db->query($sql, [':id' => $id]);
        
        if (!empty($comment)) {
            $author_id = $comment[0]['author_id'];
            $result = $this->db->query('SELECT * from author where id = :id', [':id' => $author_id]);
            
            return $result[0]['name'];
        }
        
        return null;
    }
    
    public function create($post_id, $content, $author_id) {
        
        $sql = 'INSERT INTO comment (post_id, content, author_id) VALUES (:post_id, :content, :author_id)';
        $result = $this->db->query($sql, [
            ':post_id' => $post_id,
            ':content' => $content,
            ':author_id' => $author_id,
        ]);
        
        return $result;
    }
}
