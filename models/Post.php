<?php

require_once (ROOT . 'models/AppModel.php');

class Post extends AppModel {

    public function __construct() {
        parent::__construct();
        
    }
    
    public function getPosts() {
        
        //TODO: crate limit const
        $sql = 'SELECT * FROM post order by id desc limit 100';
        $posts = $this->db->query($sql);
        return $posts;
    }
    
    public function getFavoritePosts() {
                
        //TODO: $this->db->bindValue(':count_posts', (int) COUNT_FAVORITE_POST), PDO::PARAM_INT);
        $sql = 'select count(post_id) as c, post_id from comment group by post_id order by c desc limit 5';
        $result = $this->db->query($sql);
  
        if (empty($result)) {
            return [];
        }
        $ids = [];
        
        foreach ($result as $row) {
            $ids[] = $row['post_id'];
        }
        $posts = $this->db->query('SELECT * from post WHERE id IN (' . implode(', ', $ids) . ')');
        
        return $posts ?? [];
    }
    
    public function getPostById($id) {
        
        if (empty($id)) { 
            return [];
        }
        $sql = 'SELECT * from post WHERE id = :id';
        $post = $this->db->query($sql, [':id' => $id]);
            
        return $post[0];
    }
    
    public function getShortText($string) {
        
        if (!empty($string)) {
            $string = strip_tags($string);
            $string = mb_substr($string, 0, COUNT_CHAR_FO_SHORT_CONTENT, "UTF-8");
            $string = rtrim($string, "!,.-");
            
            return $string.'... ';
        }
        
        return '';
    }
    
    public function getAuthorName($id) {
        
        $sql = 'SELECT name from author WHERE id = :id';
        $result = $this->db->query($sql, [':id' => $id]);
        $name = $result[0]['name'];
        
        return $name;
    }
    
    public function getCountComments($id) {
        
        if (empty($id)) {
            return 0;
        }
        $sql = 'SELECT count(*) as c from comment WHERE post_id = :id';
        $result = $this->db->query($sql, [':id' => $id]);

        return $result[0]['c'];
    }
    
    public function create($author_id, $content) {
        
        $sql = 'INSERT INTO post (author_id, content) VALUES (:author_id, :content)';
        $result = $this->db->query($sql, [
                ':author_id' => $author_id,
                ':content' => $content,

            ]);
            
        return $result;
    }
}
