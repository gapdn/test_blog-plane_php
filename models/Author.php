<?php

require_once (ROOT . 'models/AppModel.php');

class Author extends AppModel{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getId($name) {
        
        $sql = 'SELECT id from author WHERE name = :name';
        $author = $this->db->query($sql, [':name' => $name]);
        if (!empty($author)) {
            $id = $author[0]['id'];
            return $id;
        }
        
        return null;
    }
    
    public function getName($id) {

        $sql = 'SELECT name from author WHERE id = :id';
        $author = $this->db->query($sql, [':id' => $id]);
        
        if (!empty($author)) {
            $name = $author[0]['name'];
            return $name;
        }
        
        return null;
    }

    public function create($author_name) {
        
        $sql = "INSERT INTO author (name) VALUES (:name)";
        $result = $this->db->query($sql, [':name' => $author_name]);
        
        return $result;
    }
}
