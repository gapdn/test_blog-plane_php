
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />
        <link href="/template/css/site.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="/template/css/main.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <title>V-Jet Test</title>

    </head>
    <body class="home page">

        <div class="wrapper">
            <header>                
                <div class="header-top">
                    <div class="container">
                        <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4 brand-logo">
                            <h1>
                                <a href="post">
                                    <img src="/template/images/logo1.png" alt="">
                                </a>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="header-main-nav">
                    <div class="container">
                        <div class="main-nav-wrapper">
                            <nav class="main-menu">      

                                <ul class="menu navbar-nav navbar-right nav">
                                    <li><a href="/">Homepage</a></li>
                                    <li><a href="/about">About</a></li>
                                    <li><a href="/create">Create</a></li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

            </header>	
