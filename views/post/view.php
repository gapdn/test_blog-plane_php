<?php include_once VIEW.'/layouts/header.php'; ?>
            <div class="container full">

                <div class="page-posts no-padding">                    
                    <div class="row"> 
                        <div class="page page-post col-sm-12 col-xs-12">
                        <h1>View</h1>
                            <div class="blog-posts blog-posts-large">

                                <div class="row">
                                    <p><b>Author: </b><?php echo $author_name; ?></p>
                                    <p><b>Date create: </b><?php echo $date_create ?></p>
                                    <hr>
                                    <p><?php echo $content ?></p>
                                    <hr noshade size="1" width="75%">
                                    <p><b>Comments: </b></p>
                                    <?php if (!empty($comments)): ?>
                                        <?php foreach ($comments as $comment): ?>
                                            <p><?php echo $comment['content'] ?></p>
                                            <p><b>Leaved by: </b><?php echo $comment['author']; ?> <b>at</b> <?php echo $comment['date'] ?></p>
                                            <hr align="left" width="30%" />
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                            <p>no comments yet</p>
                                    <?php endif; ?>
                                </div>
                                <div class="row">

                                    <h3>Leave your comment</h3>
                                    <form action="/comment/create" method="post">
                                        <div class="form-group field-postform-name">
                                            <label class="control-label" for="postform-name">Name</label>
                                            <input name="name" placeholder="Enter your name" class="form-control" id="postform-name" minlength="5" required="required"/>
                                            <div class="help-block"></div>
                                        </div>
                                        <div class="form-group field-postform-content">
                                            <label class="control-label" for="postform-content">Comment</label>
                                            <textarea name="content" placeholder="Enter text" maxlength="1000" rows="3" class="form-control" id="postform-content" minlength="5"  required="required"></textarea>
                                            <div class="help-block"></div>
                                            <input type="hidden" name="post_id" value="<?php echo $post_id ?>" />
                                        </div>
                                        <input type="submit" value="Create"/>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <br>
            </div>
            <div class="push"></div>
        </div>
<?php include_once VIEW.'/layouts/footer.php'; ?>
