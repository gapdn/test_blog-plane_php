
<?php include_once VIEW.'/layouts/header.php'; ?>
            <div class="container full">

                <div class="page-posts no-padding">                    
                    <div class="row">                        
                        <div class="page page-post col-sm-12 col-xs-12">
                            <div class="blog-posts blog-posts-large">

                                <div class="row">

                                    <div class="post-default-index">

                                        <h1>Create post</h1>
                                        <form action="create" method="post">
                                             <div class="form-group field-postform-name">
                                                <label class="control-label" for="postform-name">Name</label>
                                                <input type="text" name="name" placeholder="Enter Your Name" class="form-control" id="postform-name" minlength="5" required="required">
                                                <div class="help-block"></div>
                                            </div>
                                            <div class="form-group field-postform-content">
                                                <label class="control-label" for="postform-content">Content</label>
                                                <textarea name="content" placeholder="Enter text" maxlength="1000" rows="3" class="form-control" id="postform-content" minlength="5" required="required"></textarea>
                                                <div class="help-block"></div>
                                            </div>
                                            <input type="submit" value="Create"/>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <br>
            </div>
            <div class="push"></div>
        </div>
<?php include_once VIEW.'/layouts/footer.php'; ?>