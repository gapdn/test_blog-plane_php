<?php include_once VIEW.'/layouts/header.php'; ?>
            <div class="container full">

                <?php if (!empty($fposts)): ?>
                    <!-- Carousel
                    ================================================== -->
                    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="background: aliceblue; border-radius: 5px;">
                        <ol class="carousel-indicators">
                            <?php foreach ($fposts as $key => $row): ?>
                                <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>"  <?= ($key == 0) ? 'class="active"' : ''; ?> ></li>
                            <?php endforeach; ?>
                        </ol>
                        <div class="carousel-inner" style="padding-bottom: 60px;">
                            <?php foreach ($fposts as $key => $row): ?>            
                                <div class="item <?= ($key == 0) ? 'active' : ''; ?>">
                                    <div class="row">
                                        <div class="col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8">
                                            <h2><?php echo $row; ?></h2>

                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>


                        </div>
                        <a style="background: linear-gradient(to right, aliceblue, white);" class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                        <a style="background: linear-gradient(to right, white, aliceblue);" class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    </div><!-- /.carousel -->
                <?php endif; ?>

                <div class="page-posts no-padding">                    
                    <div class="row">                        
                        <div class="page page-post col-sm-12 col-xs-12">
                            <div class="blog-posts blog-posts-large">

                                <div class="row">

                                    <?php if (!empty($posts)): ?>
                                        <?php foreach ($posts as $post): ?>

                                            <div class="post">
                                                <div class="entry">
                                                    <!--<p><img src="/template/images/pic01.jpg" width="800" height="300" alt="" /></p>-->
                                                    <p>"<?php echo $post['content'] ?>"</p>
                                                    <p>Author: <?php echo $post['name'] ?></p>
                                                    <p>Date create: <?php echo $post['date'] ?></p>
                                                    <p>Comments: <?php echo $post['comments'] ?></p>
                                                    <a href="<?php echo '/post/' . $post['id'] ?>">read more</a>
                                                    <hr>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>


                                    <?php else: ?>
                                        <div class="col-md-12">
                                            <?php echo $message ?>
                                            <a href="/create">Be the first :)</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <br>
            </div>
            <div class="push"></div>
        </div>
<?php include_once VIEW.'/layouts/footer.php'; ?>








